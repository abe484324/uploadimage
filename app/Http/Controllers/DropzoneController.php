<?php

namespace App\Http\Controllers;

use App\Models\uploadModel;
use Illuminate\Http\Request;

class DropzoneController extends Controller
{
    public function dropzone(){
        return view('dropzone');
    }

    public function dropzoneStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = time().'.'.$image->extension();
        $image->move(public_path('images'),$imageName);

        $uploadModel =new uploadModel();
        $uploadModel->image_name=$imageName;
        $uploadModel->save();
        return response()->json(['success'=>$imageName]);
    }
}
